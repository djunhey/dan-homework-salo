const books = [{
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const booksPage = document.querySelector('#root');

function booksNew() {
    let booksList = books.map(function(item) {
        let { author, name, price } = item;
        if (author && name && price) {
            return `<li>Автор: ${author}</br>Произведение: ${name}</br> Прайс: ${price}</li>`;
        }
    });
    booksPage.insertAdjacentHTML('afterbegin', `<ul>${booksList.join('')}</ul>`);
}

booksNew()

function booksError(item) {
    let { author, name, price } = item;
    if (!author) {
        throw new Error(`Введите пожалуйста имя АВТОРА книги ${name}`);
    } else if (!name) {
        throw new Error(`Введите пожалуйста название КНИГИ автора  ${author}`);
    } else if (!price) {
        throw new Error(`Введите пожалуйста стоимость книги ${name} автора ${author}`);
    }
    return item;
}


for (let i = 0; i < books.length; i++) {
    try {
        booksError(books[i]);
    } catch (el) {
        console.log(el);
    }
}