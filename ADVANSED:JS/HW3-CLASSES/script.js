class Employee {
    constructor(firstName, lastName, age, salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.salary = salary;

    }
    get fullName() {
        return `Полное имя: ${this.firstName} ${this.lastName};`
    }

    set fullName(value) {
        [this.firstName, this.lastName] = value.split(" ");
    }

    get ageUser() {
        return `Возраст: ${this.age};`
    }

    set ageUser(value) {
        this.age = value.split(" ");
    }

    get salaryUser() {
        return `Зарплата: ${this.salary}$;`
    }

    set salaryUser(value) {
        this.salary = value.split(" ");
    }

}

class Programmer extends Employee {
    constructor(firstName, lastName, age, salary, land) {
        super(firstName, lastName, age, salary);
        this.land = land;
    }

    get fullInformFull() {
        return `${this.fullName} 
${this.ageUser};
Зарплата: ${this.salary * 3}$;
Языки: ${this.land};
            `
    }

    set fullInformFull(value) {
        [this.fullName, this.ageUser, this.salary, this.land] = value.split(" ");
    }
}

const employeeOne = new Employee('Vasiliy', 'Rusanov', 35, 500)
const employeeTwo = new Programmer('Vladimir', 'Lodzinskiy', 25, 700, ['Spanish', 'English', 'French', 'Japanese'])


console.log(employeeOne.fullName, employeeOne.ageUser, employeeOne.salaryUser)
console.log(employeeTwo.fullInformFull)