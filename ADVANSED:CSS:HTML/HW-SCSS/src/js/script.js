let headerMenu = document.querySelector('.btn-open');
let headerMenuClose = document.querySelector('.btn-close');

headerMenu.addEventListener('click', (event) => {
    event.preventDefault();
    document.querySelector('.header__navigation-new').style.display = 'block';
    event.target.style.display = 'none';
    document.querySelector('.btn-close').style.display = 'block';
});

headerMenuClose.addEventListener('click', (event) => {
    event.preventDefault();
    document.querySelector('.header__navigation-new').style.display = 'none';
    document.querySelector('.btn-close').style.display = 'none';
    headerMenu.style.display = 'block';
    document.querySelector('.btn-line').style.display = 'block';
});

window.addEventListener('resize', () => {
    if (screen.width > 480) {
        headerMenu.style.display = "none";
        headerMenuClose.style.display = "none";
        document.querySelector('.header__navigation-new').style.display = 'none';
    } else if (document.querySelector('.btn-close').style.display === 'block') {
        headerMenu.style.display = 'none';
    } else {
        headerMenu.style.display = 'block';
    }
});