const userButton = document.querySelector('.btn');
const userPassword = document.querySelector('.passwordOne');
const userCheck = document.querySelector('.passwordTwo');

document.addEventListener('click', (event) => {
    if (event.target.classList.contains('icon-password')) {
        if (event.target.classList.contains('fa-eye')) {
            event.target.classList.replace('fa-eye', 'fa-eye-slash');
            event.target.previousElementSibling.type = 'text';
        } else {
            event.target.classList.replace('fa-eye-slash', 'fa-eye');
            event.target.previousElementSibling.type = 'password';
        }
    }
});

userButton.addEventListener('click', () => {
    if (userPassword.value === userCheck.value) {
        alert('You are welcome!');
    } else {
        const noCorrect = document.createElement('p');
        noCorrect.textContent = 'The corresponding identical values ​​are needed';
        noCorrect.style.color = 'red';
        document.body.append(noCorrect);
    }
});