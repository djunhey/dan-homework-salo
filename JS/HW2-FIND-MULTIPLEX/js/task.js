let userNumber = +prompt("Enter your number:");
while (userNumber == '' || isNaN(userNumber)) {
    userNumber = +prompt("Enter number, please!");
}
if (userNumber < 5) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 5; i <= userNumber; i += 5) {
        if (i % 5 == 0) {
            console.log(i);
        }
    }
}