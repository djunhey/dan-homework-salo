function redButton() {
    const buttons = document.querySelectorAll('.btn');
    document.addEventListener('keydown', function(event) {

        buttons.forEach(item => {
            if (event.key.toLowerCase() === item.textContent.toLowerCase()) {
                item.classList.add('btn-red');
            } else {
                item.classList.remove('btn-red');
            }
        })
    })
}

redButton()