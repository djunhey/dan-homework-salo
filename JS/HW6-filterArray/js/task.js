var arr = [4, 6, 'auto', null, 678, [4, 7, 8], 'Anna', undefined, 1256.6]

var type = 'string';

const filterBy = function(array, typeUser) {
    var arrNew = array.filter(i => typeof i !== typeUser);
    return arrNew;
}

const result = filterBy(arr, type)

console.log(result);