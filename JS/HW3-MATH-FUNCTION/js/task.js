let userNumberFirst = +prompt('Enter first number');
while (userNumberFirst == '' || isNaN(userNumberFirst)) {
    userNumberFirst = +prompt('Wrong data.Please enter the number again, please.');
}

let userNumberSecond = +prompt('Enter  second number ');
while (userNumberSecond == '' || isNaN(userNumberSecond)) {
    userNumberSecond = +prompt('Wrong data.Please enter the number again, please.');
}

let userTask = prompt('Enter a task of your choice: + , - , / , *');
while (
    userTask !== '+' &&
    userTask !== '-' &&
    userTask !== '/' &&
    userTask !== '*') {
    userTask = prompt("Enter correct operation");
}

function calculator(numberOne, numberTwo, userCalcul) {

    switch (userCalcul) {
        case '+':
            return numberOne + numberTwo;
        case '-':
            return numberOne - numberTwo;
        case '*':
            return numberOne * numberTwo;
        case '/':
            return numberOne / numberTwo;
        default:
            return "No result. You entered the wrong task";
    }
}

const result = calculator(userNumberFirst, userNumberSecond, userTask)
console.log('Результат: ' + result)