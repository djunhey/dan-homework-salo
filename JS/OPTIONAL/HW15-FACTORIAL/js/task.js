let usernumber = +prompt('Enter number');

while (usernumber == '' || isNaN(usernumber)) {
    usernumber = +prompt("Enter number, please!");
}

function factorial(calculator) {
    return (calculator != 1) ? calculator * factorial(calculator - 1) : 1;
}

alert(factorial(usernumber));