const createNewUser = function() {
    let userName = prompt('Enter your first name');
    while (userName == '' || !isNaN(userName)) {
        username = prompt('Wrong data. Please enter first name again, please.');
    }
    let surName = prompt('Enter your last name');
    while (surName == '' || !isNaN(surName)) {
        surName = prompt('Wrong data. Please enter last name again, please.');
    }
    let userDate = prompt('Enter your date of birth in the format: date/month/year');
    while (userDate == '') {
        userDate = prompt('Wrong data. Please enter your date of birth in the format: date/month/year again, please.');
    }

    return newDate = {
        firstName: userName,
        lastName: surName,
        birthday: new Date(userDate),

        getAge() {
            let dateNow = new Date()
            let dateArray = userDate.split('/');
            let dataArrayNew = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
            timeDiff = Math.abs(dateNow.getTime() - dataArrayNew.getTime());
            return Math.round(timeDiff / (24 * 60 * 60 * 365 * 1000) | 0);
        },
        getPassword() {
            return (this.firstName[0].toUpperCase()) + (this.lastName.toLowerCase()) + (this.birthday.getFullYear())
        }
    }
}

const newUser = createNewUser();

console.log(newUser.getPassword());
console.log('Age: ' + newUser.getAge());