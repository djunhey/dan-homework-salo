const inputStyle = document.querySelector('.userInput');

const insertPriceBlock = document.querySelector('.userPriceBlock');

const insertUserPrice = document.querySelector('.userPrice');

const priceNewBlock = document.createElement('div');
priceNewBlock.classList.add('userPriceNewItem');

const priceNewText = document.createElement('span');
const priceNewTextTwo = document.createElement('span');

const buttonDelete = document.createElement('button');
buttonDelete.textContent = 'x';
buttonDelete.classList.add('buttonStyle');

priceNewBlock.append(priceNewText, buttonDelete);


inputStyle.addEventListener('focus', () => {
    inputStyle.classList.add('inputBorderGreen');
})

inputStyle.addEventListener('blur', () => {
    if (inputStyle.value < 0 || inputStyle.value == '') {
        inputStyle.style.borderColor = '#dc143c';
        priceNewBlock.remove();
        priceNewTextTwo.textContent = `Please enter correct price`;
        priceNewTextTwo.classList.add('noCorrectNumber');
        insertPriceBlock.append(priceNewTextTwo);

    } else {
        priceNewText.textContent = `Текущая цена:   ${inputStyle.value}`
        insertPriceBlock.prepend(priceNewBlock);
        inputStyle.style.borderColor = '#000000';
        priceNewTextTwo.remove();

    }
    return
})

buttonDelete.onclick = () => {
    inputStyle.value = '';
    priceNewBlock.remove();
}